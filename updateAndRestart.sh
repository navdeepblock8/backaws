set -e
echo "update"
# Delete the old repo
rm -r backaws

# clone the repo again
git clone https://gitlab.com/navdeepblock8/backaws

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH
source ./.nvm/nvm.sh

# stop the previous pm2
pm2 kill
npm remove pm2 -g


#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
npm install pm2 -g
# starting pm2 daemon
pm2 status

cd backaws

#install npm packages
echo "Running npm install"
npm install

#Restart the node server
pm2 start index.js 

pm2 save
